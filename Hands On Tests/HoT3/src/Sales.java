import java.util.Scanner;
import java.io.*;
import java.text.DecimalFormat;
public class Sales {
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		Scanner scan = new Scanner(System.in);
		PrintWriter writer = new PrintWriter("src\\WeeklySales.txt", "UTF-8");
		DecimalFormat dform = new DecimalFormat("###,###,##0.00");
		double sales=0;
		double value;
		
		for (int i=0; i<5;i++) {
			System.out.println("Please enter the amount of sales for day "+(i+1));
			value = scan.nextDouble();
			while (value <0) {
				System.out.println("The value must be 0 or higher. Try again");
				value = scan.nextDouble();
			}
			writer.println("Day "+(i+1)+": $"+dform.format(value));
			sales += value;
		}
		writer.println("Total: $"+dform.format(sales));
		writer.close();
	}
}
