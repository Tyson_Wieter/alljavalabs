
public class DiceGame {
	public static void main(String[] args) {
		Player p1 = new Player();
		Player p2 = new Player();
		boolean rolling=true;
		while(rolling) {
			System.out.print("Player 1: ");
			System.out.println(p1.rollDie());
			System.out.print("Player 2: ");
			System.out.println(p2.rollDie());
			if(p1.getPoints() == 1) {
				System.out.println("Player 1 Wins!!");
				rolling = false;
			}
			else if (p2.getPoints()==1) {
				System.out.println("Player 2 Wins!!");
				rolling = false;
			}
		}
	}
}
