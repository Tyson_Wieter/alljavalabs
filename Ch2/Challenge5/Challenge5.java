import java.util.Scanner;

class Challenge5{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		double calories=0;
		double consumed;
		
		System.out.println("Enter how many cookies you ate");
		consumed = scan.nextDouble();
		calories = 300 * (consumed/4);
		System.out.println("You ate "+calories+" calories!");
	}
}