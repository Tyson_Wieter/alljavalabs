
public class VehicleDriver {
	public static void main(String[] args) {
		//creates a Vehicle then upcasts it to a car
		Vehicle v1 = new Car();
		//display results
		System.out.println(v1.getSpeed());
		v1.accelerate();
		System.out.println(v1.getSpeed());
		//takes the prior object then upcasts it to a truck
		v1 = new Truck();
		//display results
		System.out.println(v1.getSpeed());
		v1.accelerate();
		System.out.println(v1.getSpeed());
	}
}
