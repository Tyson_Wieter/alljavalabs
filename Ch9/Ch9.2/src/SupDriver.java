import java.text.DecimalFormat;
public class SupDriver {
	public static void main(String[] args) {
		DecimalFormat dform = new DecimalFormat("###,###,###,###,##0.00");
		ShiftSupervisor emp1 = new ShiftSupervisor("Tyson", "111-T", "1/2/2018", 42000,1000);
		System.out.println("Name: "+emp1.getName());
		System.out.println("Employee Number: "+emp1.getEmployeeNumber());
		System.out.println("Hire Date: "+emp1.getHireDate());
		System.out.println("Salary: $"+dform.format(emp1.getSalary()));
		System.out.println("Bonus: $"+dform.format(emp1.getBonus()));
	}
}
