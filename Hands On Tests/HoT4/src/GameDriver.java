public class GameDriver {
	public static void main(String[] args) {
		//instantiating Game object
		Game g1 = new Game("Super Mario", 2);
		//Instantiating GameWithTimeLimit object
		GameWithTimeLimit g2 = new GameWithTimeLimit("Zelda",1,30);
		//Outputting Results of both objects
		System.out.println(g1.toString());
		System.out.println(g2.toString());
	}
}
