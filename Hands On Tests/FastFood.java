import java.util.Scanner;

class FastFood{
	public static void main(String[] args){
		// Initializing variables for items, name, and scanner
		Scanner scan = new Scanner(System.in);
		int hamburg, cheeseburg, soda, fries;
		double hamPrice, cheesePrice, sodaPrice, friePrice, total;
		String name;

		// Getting inputs for all the items and the name
		System.out.println("How many hamburgers would you like to order?");
		hamburg = scan.nextInt();	
		System.out.println("How many cheeseburgers would you like to order?");
		cheeseburg = scan.nextInt();
		System.out.println("How many sodas would you like to order?");
		soda = scan.nextInt();
		System.out.println("How many fries would you like to order?");
		fries = scan.nextInt();
		System.out.println("Please enter your name");
		scan.nextLine();
		name = scan.nextLine();
		
		// Doing math to get the total
		hamPrice = hamburg*1.25;
		cheesePrice = cheeseburg*1.5;
		sodaPrice = soda*1.95;
		friePrice = fries*0.95;
		total = hamPrice+cheesePrice+sodaPrice+friePrice;
	
		// Outputting results
		System.out.println("\n\n"+name.toUpperCase());
		System.out.println("-------------------------------");
		System.out.printf("Total: $%,.2f",total);
	}
}