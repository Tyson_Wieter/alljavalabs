
public class Rainfall {
	private double[] rain = new double[12];
	double total=0;
	
	public Rainfall(double[] rain) {
		for (int i=0; i<rain.length;i++)
			this.rain[i]=rain[i];
	}
	
	public double rainForYear() {
		for (int i = 0; i< rain.length; i++) {
			total += rain[i];
		}
		return total;
	}
	public double avgRain() {
		total = 0;
		rainForYear();
		total = total/12;
		return total;
	}
	public int highRain() {
		int high=0;
		for (int i=0;i<rain.length;i++) {
			if (rain[i]>high)
				high = i+1;
		}
		return high;
	}
	public int lowRain() {
		int low=99999;
		for (int i=0;i<rain.length;i++) {
			if (rain[i]<low)
				low = i+1;
		}
		return low;
	}
}
