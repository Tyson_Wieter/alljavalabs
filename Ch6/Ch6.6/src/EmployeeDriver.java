
public class EmployeeDriver {
	public static void main(String[] args) {
		Employee emp1 = new Employee("Tyson","IT","Tech",104883);
		Employee emp2 = new Employee("Jim",482938);
		Employee emp3 = new Employee();
		System.out.println(emp1.getEmpName() + emp1.getEmpID()+emp1.getPos()+emp1.getDep());
		System.out.println(emp2.getEmpName() + emp2.getEmpID()+emp2.getPos()+emp2.getDep());
		System.out.println(emp3.getEmpName() + emp3.getEmpID()+emp3.getPos()+emp3.getDep());
	}
}
