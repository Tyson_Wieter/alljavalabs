import java.util.Scanner;

class Challenge9{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		double miles;
		double gasUsed;
		double milesPerGallon;

		System.out.println("Enter the number of miles driven and number of gallons of gas used seperated by pressing enter");
		miles = scan.nextDouble();
		gasUsed = scan.nextDouble();
		milesPerGallon = miles/gasUsed;
		System.out.println("Your MPG is " + milesPerGallon);
	}
}