import java.util.Scanner;
import java.io.*;
public class DFileDriver {
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		Scanner scan = new Scanner(System.in);
		PrintWriter writer = new PrintWriter("src\\DFile.txt", "UTF-8");
		int time =0, speed=0;
		boolean valid = true;
		System.out.println("Enter the amount of hours the trip will take and the speed the vehicle is going seperated by pressing enter.");
		while(valid) {
			time = scan.nextInt();
			speed = scan.nextInt();
			if(time <0 || speed<0) {
				System.out.println("Values must be more than 0");
				valid = true;
			}
			else 
				valid =false;
		}
		DFile trip1 = new DFile(time, speed);
		for (int i=1; i<= time; i++) {
			writer.println(trip1.getDistance(i));
			
		}
		writer.close();
	}
}
