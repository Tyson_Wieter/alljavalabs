import java.util.Scanner;

public class Challenge1 {
	public static void main(String[] args) {
		Employee num1 = new Employee();
		num1.setName("Susan Meyers");
		num1.setId(47899);
		num1.setDep("Accounting");
		num1.setPos("Vice President");
		Employee num2 = new Employee();
		num2.setName("Mark Jones");
		num2.setId(39119);
		num2.setDep("IT");
		num2.setPos("Programmer");
		Employee num3 = new Employee();
		num3.setName("Joy Rogers");
		num3.setId(81774);
		num3.setDep("Manufacturing");
		num3.setPos("Engineer");
		System.out.println("Name:"+num1.getName()+"| ID:"+num1.getNum()+"| Department:"+num1.getDep()+"   | Position:"+num1.getPosition());
		System.out.println("Name:"+num2.getName()+"  | ID:"+num2.getNum()+"| Department:"+num2.getDep()+"           | Position:"+num2.getPosition());
		System.out.println("Name:"+num3.getName()+"  | ID:"+num3.getNum()+"| Department:"+num3.getDep()+"| Position:"+num3.getPosition());
	}
}
