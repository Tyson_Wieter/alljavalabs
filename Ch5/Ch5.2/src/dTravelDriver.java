import java.util.Scanner;
public class dTravelDriver {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double time =0, speed=0;
		boolean valid = true;
		System.out.println("Enter the amount of hours the trip will take and the speed the vehicle is going seperated by pressing enter.");
		while(valid) {
			time = scan.nextDouble();
			speed = scan.nextDouble();
			if(time <0 || speed<0) {
				System.out.println("Values must be more than 0");
				valid = true;
			}
			else 
				valid =false;
		}
		dTraveled trip1 = new dTraveled(time, speed);
		for (int i=1; i<= time; i++) {
			System.out.println(trip1.getDistance(i));
			
		}
	}
}
