import java.text.DecimalFormat;
import java.util.Scanner;
public class LawnDriver {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		double length1, width1, length2, width2, totalCost1 = 0, totalCost2 = 0;
		System.out.println("Please enter the length and a width of the first lawn seperated by pressing enter");
		length1 = scan.nextDouble();
		width1 = scan.nextDouble();
		System.out.println("Please enter the length and a width of the second lawn seperated by pressing enter");
		length2 = scan.nextDouble();
		width2 = scan.nextDouble();
		Lawn lawn1 = new Lawn(length1, width1);
		Lawn lawn2 = new Lawn();
		lawn2.setLawnLength(length2);
		lawn2.setLawnWidth(width2);
		if (lawn1.squareFeet()<400) {
			totalCost1 = 25;
		}
		else if (lawn1.squareFeet()>=400 && lawn1.squareFeet()<600) {
			totalCost1 = 35;
		}
		else if (lawn1.squareFeet() >= 600) {
			totalCost1 = 50;
		}
		if (lawn2.squareFeet()<400) {
			totalCost2 = 25;
		}
		else if (lawn2.squareFeet()>=400 && lawn2.squareFeet()<600) {
			totalCost2 = 35;
		}
		else if (lawn2.squareFeet() >= 600) {
			totalCost2 = 50;
		}
		DecimalFormat dFormat = new DecimalFormat("###,###,##0.00");
		System.out.println("The weekly cost for lawn 1: $" +dFormat.format(totalCost1));
		System.out.println("The total cost for lawn 1: $" +dFormat.format(totalCost1 * 20));
		System.out.println("The weekly cost for lawn 2: $" +dFormat.format(totalCost2));
		System.out.println("The total cost for lawn 2: $" +dFormat.format(totalCost2 * 20));
	}
}
