
public class RoomDriver {
	public static void main(String[] args) {
		RoomDimension rd = new RoomDimension(5,5);
		
		RoomCarpet rc = new RoomCarpet(rd,8);
		
		System.out.println(rc.totalCost());
	}
}
