
public class Library {
	private Book book;
	private String name;
	
	public Library(Book book, String name) {
		this.book = book;
		this.name = name;
	}
	public String toString() {
		return "Library: "+name+"\nBook: "+book.toString();
	}
}
