
public class Lawn {
	private double lawnLength, lawnWidth;
	public Lawn(double length, double width) {
		lawnLength = length;
		lawnWidth = width;
	}
	public Lawn() {
	}
	public void setLawnLength(double length) {
		this.lawnLength = length;
	}
	public void setLawnWidth(double width) {
		this.lawnWidth = width;
	}
	public double getLawnLength() {
		return lawnLength;
	}
	public double getLawnWidth() {
		return lawnWidth;
	}
	public double squareFeet() {
		return lawnLength*lawnWidth;
	}
	
}
