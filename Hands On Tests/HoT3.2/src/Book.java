
public class Book {
	private String name;
	private int date;
	private String author;
	
	public Book(String name, String author, int date) {
		this.name = name;
		this.date = date;
		this.author = author;
	}
	public String toString() {
		return name +" "+date+" By "+author;
	}
	public boolean equals(Object obj) {
	    if (obj == null) {
	        return false;
	    }
}
