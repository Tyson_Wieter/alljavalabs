import java.util.Scanner;

class Challenge10{
	public static void main(String[] args){
		double test1, test2, test3, avg;		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter 3 test scores seperated by pressing enter.");
		test1 = scan.nextDouble();
		test2 = scan.nextDouble();
		test3 = scan.nextDouble();
		avg = (test1+test2+test3)/3;
		System.out.printf("Test 1: %.2f",test1);
		System.out.printf("\nTest 2: %.2f",test2);
		System.out.printf("\nTest 3: %.2f",test3);
		System.out.printf("\nTest Averages: %.2f",avg);
	}
}