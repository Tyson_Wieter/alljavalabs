import java.util.Scanner;

class Challenge12{
	public static void main(String[] args){		
		Scanner scan = new Scanner(System.in);
		String city;

		System.out.println("Enter a city name");
		city = scan.nextLine();
		System.out.println(city.length());
		System.out.println(city.toUpperCase());	
		System.out.println(city.toLowerCase());
		System.out.println(city.substring(0,1));	
	}
}