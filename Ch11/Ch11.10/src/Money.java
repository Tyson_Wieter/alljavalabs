import java.util.Random;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Money {
	public static class MyFirstGUI extends Application
	{
		private Image image = new Image("file:C:\\Users\\Doge\\Documents\\Games\\IWT\\JavaLabs\\alljavalabs\\Ch11\\CH11.5\\Heads.gif");
		private Image image2 = new Image("file:C:\\Users\\Doge\\Documents\\Games\\IWT\\JavaLabs\\alljavalabs\\Ch11\\CH11.5\\Tails.gif");
		private ImageView imageView = new ImageView(image);
		public static void main(String[] args)
	    {
			launch(args);
	    }
	    @Override
	    public void start(Stage primaryStage)
	    {
	    	primaryStage.setTitle("Coin Flip");
	    	
	    	Button but1 = new Button("Flip");
	    	but1.setOnAction(handle1);
	    	VBox vBox = new VBox(imageView, but1);
	    	Scene scene = new Scene(vBox, 500, 500);
	    	primaryStage.setScene(scene);
	    	primaryStage.show();
	    }
	    final EventHandler<ActionEvent> handle1 = new EventHandler<ActionEvent>() {
	    	@Override
			  public void handle(ActionEvent textChange)
			  {
	    		Random rand = new Random();
	    		int num = rand.nextInt(2);
	    		System.out.println(num);
	    		if(num==1) {
	    			imageView.setImage(image);
	    			System.out.println("Heads");
	    		}
	    		else if(num==0) {
	    			imageView.setImage(image2);
	    			System.out.println("Tails");
	    		}
			  }
	    };
	}
}
