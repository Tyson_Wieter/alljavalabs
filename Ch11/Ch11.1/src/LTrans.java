import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.Button;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
public class LTrans {
	public static class MyFirstGUI extends Application
	{
		private Label lab1,lab2,lab3;
		public static void main(String[] args)
	    {
			launch(args);
	    }
	
	    @Override
	    public void start(Stage primaryStage)
	    {
	    	primaryStage.setTitle("The Latin Translator");
	    	Button but1 = new Button("Sinister");
	    	Button but2 = new Button("Dexter");
	    	Button but3 = new Button("Medium");
	    	but1.setOnAction(handle1);
	    	but2.setOnAction(handle2);
	    	but3.setOnAction(handle3);
	    	lab1 = new Label();
	    	lab2 = new Label();
	    	lab3 = new Label();
	    	VBox vbox = new VBox(10, but1, but2, but3);
	    	VBox vbox2 = new VBox(20,lab1,lab2,lab3);
	    	HBox hbox = new HBox(10,vbox,vbox2);
	    	Scene scene = new Scene(hbox, 100, 100);
	    	hbox.setAlignment(Pos.CENTER_LEFT);
	    	primaryStage.setScene(scene);
	    	primaryStage.show();
	  }
	    final EventHandler<ActionEvent> handle1 = new EventHandler<ActionEvent>() {
	    	@Override
			  public void handle(ActionEvent textChange)
			  {
			    lab1.setText("Left");
			  }
	    };
	    final EventHandler<ActionEvent> handle2 = new EventHandler<ActionEvent>() {
	    	@Override
			  public void handle(ActionEvent textChange)
			  {
			    lab2.setText("Right");
			  }
	    };
	    final EventHandler<ActionEvent> handle3 = new EventHandler<ActionEvent>() {
	    	@Override
			  public void handle(ActionEvent textChange)
			  {
			    lab3.setText("Center");
			  }
	    };
	}
}
