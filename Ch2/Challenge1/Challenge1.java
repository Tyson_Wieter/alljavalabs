class Challenge1
{
	public static void main(String[] args)
	{
		String name = "Tyson Wieter";
		int age = 20;
		double annualPay = 100000;

		System.out.println("My name is "+name+", my age is "+age+", and I hope to earn $"+annualPay+" per year");
	}
}