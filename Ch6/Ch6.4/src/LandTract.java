
public class LandTract {
	private double length, width;
	public LandTract(double length,double width) {
		this.length = length;
		this.width = width;
	}
	public double getArea() {
		return length*width;
	}
	public boolean equals(LandTract plot) {
		if(plot.getArea() == this.getArea())
			return true;
		else
			return false;
	}
}
