import java.util.Random;
public class Dice {
	private int sides;
	private int dRoll;
	Random rand = new Random();
	public Dice(int sides) {
		this.sides = sides;
	}
	public int roll() {
		dRoll = rand.nextInt(sides);
		return dRoll+1;
	}
}
