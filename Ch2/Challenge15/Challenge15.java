import java.util.Scanner;

class Challenge15{
	public static void main(String[] args){		
		Scanner scan = new Scanner(System.in);
		double sugarCup=1.5, butterCup=1, flourCup=2.75, cookWant, total;

		System.out.println("How many cookies do you want?");
		cookWant = scan.nextDouble();
		total = cookWant/48;
		sugarCup = sugarCup*total;
		butterCup = butterCup*total;
		flourCup = flourCup*total;
		System.out.println("Cups of Sugar: "+sugarCup);
		System.out.println("Cups of Butter: "+butterCup);
		System.out.println("Cups of Flour: "+flourCup);
	}
}