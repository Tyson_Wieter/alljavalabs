
public class Employee {
	private String empName, dep, pos;
	private int empID;
	public Employee (String empName, String dep, String pos, int empID) {
		this.empID = empID;
		this.empName = empName;
		this.dep = dep;
		this.pos = pos;
	}
	public Employee (String empName,int empID) {
		this.empID = empID;
		this.empName = empName;
		this.dep = "";
		this.pos = "";
	}
	public Employee () {
		this.empID = 0;
		this.empName = "";
		this.dep = "";
		this.pos = "";
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getDep() {
		return dep;
	}
	public void setDep(String dep) {
		this.dep = dep;
	}
	public String getPos() {
		return pos;
	}
	public void setPos(String pos) {
		this.pos = pos;
	}
	public int getEmpID() {
		return empID;
	}
	public void setEmpID(int empID) {
		this.empID = empID;
	}
}
