
public class ProductionWorker extends Employee{
	private int shift;
	private double payRate;
	public final int DAY_SHIFT = 1;
	public final int NIGHT_SHIFT = 2;
	public ProductionWorker(String n, String num, String date, int shift, double payRate) {
		super(n, num, date);
		this.shift = shift;
		this.payRate = payRate;
	}
	public ProductionWorker() {
		
	}
	public int getShift() {
		return shift;
	}
	public void setShift(int shift) {
		this.shift = shift;
	}
	public double getPayRate() {
		return payRate;
	}
	public void setPayRate(double payRate) {
		this.payRate = payRate;
	}
	public int getDAY_SHIFT() {
		return DAY_SHIFT;
	}
	public int getNIGHT_SHIFT() {
		return NIGHT_SHIFT;
	}
}
