import java.util.Scanner;
public class LandTractDriver {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a length and then a width for plot 1 seperated by pressing enter");
		LandTract plot1 = new LandTract(scan.nextDouble(),scan.nextDouble());
		System.out.println("Enter a length and then a width for plot 2 seperated by pressing enter");
		LandTract plot2 = new LandTract(scan.nextDouble(),scan.nextDouble());
		System.out.println("Plot 1: "+plot1.getArea());
		System.out.println("Plot 2: "+plot2.getArea());
		if (plot1.equals(plot2))
			System.out.println("The Plots of land are equal");
		else
			System.out.println("The plots of land are not equal");
	}
}
