
public class TeamLeader extends ProductionWorker{
	private double monthlyBonus;
	private double requiredTrainingHours;
	public double trainingHoursAttended;
	
	
	public TeamLeader(String n, String num, String date, int shift, double payRate, double monthlyBonus,
			double requiredTrainingHours, double trainingHoursAttended) {
		super(n, num, date, shift, payRate);
		this.monthlyBonus = monthlyBonus;
		this.requiredTrainingHours = requiredTrainingHours;
		this.trainingHoursAttended = trainingHoursAttended;
	}

	public TeamLeader() {
	}

	public double getMonthlyBonus() {
		return monthlyBonus;
	}

	public void setMonthlyBonus(double monthlyBonus) {
		this.monthlyBonus = monthlyBonus;
	}

	public double getRequiredTrainingHours() {
		return requiredTrainingHours;
	}

	public void setRequiredTrainingHours(double requiredTrainingHours) {
		this.requiredTrainingHours = requiredTrainingHours;
	}

	public double getTrainingHoursAttended() {
		return trainingHoursAttended;
	}

	public void setTrainingHoursAttended(double trainingHoursAttended) {
		this.trainingHoursAttended = trainingHoursAttended;
	}

	@Override
	public String toString() {
		return "TeamLeader [monthlyBonus=" + monthlyBonus + ", requiredTrainingHours=" + requiredTrainingHours
				+ ", trainingHoursAttended=" + trainingHoursAttended + ", DAY_SHIFT=" + DAY_SHIFT + ", NIGHT_SHIFT="
				+ NIGHT_SHIFT + "]";
	}
}
