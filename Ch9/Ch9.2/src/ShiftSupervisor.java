
public class ShiftSupervisor extends Employee {
	private double salary;
	private double bonus;
	
	public ShiftSupervisor(String n, String num, String date, double salary, double bonus) {
		super(n, num, date);
		this.salary = salary;
		this.bonus = bonus;
	}
	public ShiftSupervisor() {
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public double getBonus() {
		return bonus;
	}
	public void setBonus(double bonus) {
		this.bonus = bonus;
	}
	public String toString() {
		return "";
	}
}
