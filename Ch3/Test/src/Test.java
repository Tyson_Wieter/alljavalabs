import java.util.Scanner;
import java.lang.Math;

public class Test {
	public static int[] makeCharacter() {
		int max = 7, min=1;
		int[] stat = new int[4];
		for(int i =0;i<4;i++) {
			double randNum = min + (Math.random() * (max - min));
			int randy = (int)randNum;
			stat[i] = randy;
		}
			return stat;
	}
	public static int[] oneStat() {
		int[] stat = makeCharacter();
		int lowest = stat[0];
		for (int i = 1; i < stat.length; i++) 
		{
		    if(lowest > stat[i]) 
		    {
		       lowest = stat[i];
		    }
		}
		return stat;
	}
	public static int[] maxStat() {
		System.out.println("Which Stat would you like to be highest");
		Scanner scan = new Scanner(System.in);
		int highStat = scan.nextInt();
		int[] finishedStats = {0,0,0,0,0,0};
		for (int j=0;j<6;j++) {
			int[] stat = oneStat();
			for (int i=0;i<3;i++) {
				finishedStats[j] += (stat[i]);
			}
		}
		int highest = finishedStats[0];
		for (int i = 1; i < finishedStats.length; i++) 
		{
		    if(highest < finishedStats[i]) 
		    {
		       highest = finishedStats[i];
		    }
		}
		if (highStat == 0) {
			finishedStats[0] = highest;
		}
		else if (highStat == 1) {
			finishedStats[1] = highest;
		}
		else if (highStat == 2) {
			finishedStats[2] = highest;
		}
		else if (highStat == 3) {
			finishedStats[3] = highest;
		}
		else if (highStat == 4) {
			finishedStats[4] = highest;
		}
		else if (highStat == 5) {
			finishedStats[5] = highest;
		}
		else {
			System.out.println("ERROR");
		}
		return finishedStats;
	}
	public static String makeName() {
		String[] beg = {"A","Ada","Aki","Al","Ali","Alo","Ana","Ani","Ba","Be","Bo","Bra","Bro","Cha","Chi","Da","De","Do","Dra","Dro","Eki","Eko","Ele","Eli","Elo","Er","Ere","Eri","Ero","Fa","Fe","Fi","Fo","Fra","Gla","Gro","Ha","He","Hi","Illia","Ira","Ja","Jo","Ka","Ki","Kra","La","Le","Lo","Ma","Me","Mi","Mo","Na","Ne","No","O","Ol","Or","Pa","Pe","Pi","Po","Pra","Qua","Qui","Quo","Ra","Re","Ro","Sa","Sca","Sco","Se","Sha","She","Sho","So","Sta","Ste","Sti","Stu","Ta","Tha","The","Tho","Ti","To","Tra","Tri","Tru","Ul","Ur","Va","Vo","Wra","Xa","Xi","Zha","Zho"};
		String[] mid = {"bb","bl","bold","br","bran","can","carr","ch","cinn","ck","ckl","ckr","cks","dd","dell","dr","ds","fadd","fall","farr","ff","fill","fl","fr","genn","gg","gl","gord","gr","gs","h","hall","hark","hill","hork","jenn","kell","kill","kk","kl","klip","kr","krack","ladd","land","lark","ld","ldr","lind","ll","ln","lord","ls","matt","mend","mm","ms","nd","nett","ng","nk","nn","nodd","ns","nt","part","pelt","pl","pp","ppr","pps","rand","rd","resh","rd","resh","rn","rp","rr","rush","salk","sass","sc","sh","sp","ss","st","tall","tend","told","v","vall","w","wall","wild","will","x","y","yang","yell","z","zenn"};
		String[] end = {"a","ab","ac","ace","ach","ad","adle","af","ag","ah","ai","ak","aker","al","ale","am","an","and","ane","ar","ard","ark","art","ash","at","ath","ave","ea","eb","ec","ech","ed","ef","eh","ek","el","elle","elton","em","en","end","ent","enton","ep","er","esh","ess","ett","ic","ich","id","if","ik","il","im","in","ion","ir","is","ish","it","ith","ive","ob","och","od","odin","oe","of","oh","ol","olen","omir","or","orb","org","ort","os","osh","ot","oth","ottle","ove","ow","ox","ud","ule","umber","un","under","undle","unt","ur","us","ust","ut","","","","",};
		int min = 0;
		int max = 100;
		double randNum1 = min + (Math.random() * (max - min));
		double randNum2 = min + (Math.random() * (max - min));
		double randNum3 = min + (Math.random() * (max - min));
		String name = beg[(int) randNum1]+mid[(int) randNum2]+end[(int) randNum3];
		return name;
	}
	public static void main(String[] args) {
		boolean nope = true;
		int[] stats = new int[6];
		do {
			stats = maxStat();
			for (int i=0;i<stats.length;i++) {
				if(stats[i]>14) {
					nope = false;
				}
			}
		}while (nope);
		System.out.println(makeName());
		for(int i=0;i<6;i++) {
			System.out.println(stats[i]);
		}
	}
}
