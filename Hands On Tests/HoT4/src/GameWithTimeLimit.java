
public class GameWithTimeLimit extends Game{
	//field that gives a time limit
	private int timeLimit;

	//Constructor to get all fields for this class and its parent
	public GameWithTimeLimit(String name, int maxPlayers, int timeLimit) {
		super(name, maxPlayers);
		this.timeLimit = timeLimit;
	}
	//default constructor
	public GameWithTimeLimit() {
	}
	//getter for timeLimit
	public int getTimeLimit() {
		return timeLimit;
	}
	//setter for timeLimit
	public void setTimeLimit(int timeLimit) {
		this.timeLimit = timeLimit;
	}
	//overridden toString from Game class
	@Override
	public String toString() {
		return super.toString()+ ", Time Limit: " + timeLimit;
	}
}
