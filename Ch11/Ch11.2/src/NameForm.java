import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import java.text.DecimalFormat;
public class NameForm {
	public static class MyFirstGUI extends Application
	{
		private Label lab1,lab2,lab3,lab4;
		private TextField tField;
		private DecimalFormat dform = new DecimalFormat("###,###,###,##0.00");
		public static void main(String[] args)
	    {
			launch(args);
	    }
	    @Override
	    public void start(Stage primaryStage)
	    {
	    	primaryStage.setTitle("Tip, Tax, and Total");
	    	tField = new TextField("Food Charge");
	    	lab1 = new Label();
	    	lab2 = new Label();
	    	lab3 = new Label();
	    	lab4 = new Label();
	    	Button but1 = new Button("Submit");
	    	but1.setOnAction(handle1);
	    	VBox vbox = new VBox(tField,but1,lab1,lab2,lab3,lab4);
	    	Scene scene = new Scene(vbox, 200, 100);
	    	primaryStage.setScene(scene);
	    	primaryStage.show();
	    }
	    final EventHandler<ActionEvent> handle1 = new EventHandler<ActionEvent>() {
	    	@Override
			  public void handle(ActionEvent textChange)
			  {
	    		double price = Double.parseDouble(tField.getText());
	    		lab1.setText("Tip: $"+dform.format(price*.18));
	    		lab2.setText("Sales Tax: $"+dform.format(price*.07));
	    		lab3.setText("Total: $"+dform.format(price+price*.18+price*.07));
			  }
	    };
    }
	
}
