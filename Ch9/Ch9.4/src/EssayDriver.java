
public class EssayDriver {
	public static void main(String[] args) {
		Essay john = new Essay();
		john.setContent(20);
		john.setCorrectLength(20);
		john.setGrammer(25);
		john.setSpelling(20);
		john.setScore(john.getContent()+john.getCorrectLength()+john.getGrammer()+john.getSpelling());
		System.out.println(john.getGrade());
	}
}
