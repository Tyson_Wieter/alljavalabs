import java.util.Scanner;
public class SumONum {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter a whole number above 0");
		int number = scan.nextInt();
		int sum = 0;
		for(int i=1; i<=number; i++) {
			sum += i;
		}
		System.out.println(sum);
	}
}
