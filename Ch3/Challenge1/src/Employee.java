import java.util.Scanner;
public class Employee{
	public String name;
	public int idNumber;
	public String department;
	public String position;
	public void setName(String iName) {
		name = iName;
	}
	public void setId(int iIdNumber) {
		idNumber = iIdNumber;
	}
	public void setDep(String iDep) {
		department = iDep;
	}
	public void setPos(String iPos) {
		position = iPos;
	}
	public int getNum() {
		return idNumber;
	}
	public String getDep() {
		return department;
	}
	public String getPosition() {
		return position;
	}
	public String getName() {
		return name;
	}
}
