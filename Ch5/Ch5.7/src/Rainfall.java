import java.util.Scanner;
public class Rainfall {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int years, rainfall=0;
		
		System.out.println("How many years?");
		years = scan.nextInt();
		for (int i=0; i<years;i++) {
			for(int j=0;j<12;j++) {
				System.out.println("Enter the inches of rainfall this month");
				rainfall += scan.nextInt();
			}
		}
		System.out.println("Months: "+years*12);
		System.out.println("Total Rainfall: "+rainfall +" Inches");
		System.out.println("Average Rainfall: "+rainfall/(years*12));
	}
}
