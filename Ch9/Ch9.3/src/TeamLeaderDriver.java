
public class TeamLeaderDriver {
	public static void main(String[] args) {
		TeamLeader emp1 = new TeamLeader("Tyson", "123-A", "1/2/2018", 1, 20, 300, 20, 10);
		TeamLeader emp2 = new TeamLeader();
		System.out.println("Name: " + emp1.getName());
		System.out.println("Employee Number: " + emp1.getEmployeeNumber());
		System.out.println("Hire Date: " + emp1.getHireDate());
		System.out.println("Shift: " + emp1.getShift());
		System.out.println("Pay Rate: " + emp1.getPayRate());
		System.out.println("Monthly Bonus: " + emp1.getMonthlyBonus());
		System.out.println("Required Training: " + emp1.getRequiredTrainingHours());
		System.out.println("Training Hours Attended: " + emp1.getTrainingHoursAttended());
	}
}
