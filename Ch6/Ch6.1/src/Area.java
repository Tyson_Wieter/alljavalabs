
public class Area {
	static double t = 1;
	public static double shape(double radius) {
		double area = ((Math.PI*radius)*(Math.PI*radius));
		return area;
	}
	public static double shape(double length, double width) {
		double area = length*width;
		return area;
	}
	public static double shape(double radius, double height, double pi) {
		double area = Math.pow(pi*radius,2)*height;
		return area;
	}

}
