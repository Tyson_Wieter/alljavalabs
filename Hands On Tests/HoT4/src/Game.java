public class Game {
	//Field for the name of the game
	private String name;
	//Field for the maximum amount of people who can play
	private int maxPlayers;
	
	//Constructor to get the fields
	public Game(String name, int maxPlayers) {
		this.name = name;
		this.maxPlayers = maxPlayers;
	}
	
	//default constructor
	public Game() {
	}
	
	//Getter for name
	public String getName() {
		return name;
	}
	//Setter for name
	public void setName(String name) {
		this.name = name;
	}
	//Getter for maxPlayers
	public int getMaxPlayers() {
		return maxPlayers;
	}
	//Setter for maxPlayers
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}
	//Overridden toString from Object class
	@Override
	public String toString() {
		return "Name: " + name + ", Max Players: " + maxPlayers;
	}
}
