import java.util.Scanner;

class Challenge17{
	public static void main(String[] args){		
		Scanner scan = new Scanner(System.in);
		String name, age, city, college, profession, animal, petName;

		System.out.println("Enter your name, age, city, college, profession, favorite animal, and pets name seperated by pressing enter");
		name = scan.nextLine();
		age = scan.nextLine();
		city = scan.nextLine();
		college = scan.nextLine();
		profession = scan.nextLine();
		animal = scan.nextLine();
		petName = scan.nextLine();
		System.out.println("There once was a person named "+name+" who lived in "+city+". At the age of "+age+". "+name+" went to college at "+college+". "+name+" graduated and went to work as a "+profession+". Then, "+name+" adopted a "+animal+" named "+petName+". They both lived happily ever after!!!");
	}
}