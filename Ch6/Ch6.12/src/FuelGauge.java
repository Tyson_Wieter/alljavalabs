
public class FuelGauge {
	private int fuel;
	public FuelGauge(int fuel) {
		this.fuel = fuel;
	}
	public int getFuel() {
		return fuel;
	}
	public void setFuel(int fuel) {
		this.fuel = fuel;
	}
	public void addFuel() {
		if (fuel <15)
			fuel++;
		else
			System.out.println("Tank is Full");
	}
	public void removeFuel() {
		if (fuel >0)
			fuel--;
		else
			System.out.println("Tank is Empty");
	}
}
