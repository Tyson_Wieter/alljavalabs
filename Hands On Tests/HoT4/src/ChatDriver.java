import java.util.Scanner;
import java.text.DecimalFormat;
import java.util.ArrayList;
public class ChatDriver {
	//int array that holds all of the different possible area codes
	static private int[] areaCode = {262,414,608,715,815,920};
	//double array that holds the price for above area codes
	static private double[] pMR = {.07,.10,.05,.16,.24,.14};
	//Scanner object declaration
	static Scanner scan = new Scanner(System.in);
	
	//Method to get the user input for the area code
	public static int getAreaCode() {
		System.out.println("What is the area code of your call?");
		return scan.nextInt();
	}
	
	// Method to get user input for the length of the call
	public static int getCallLength() {
		System.out.println("How long was the call in whole minutes?");
		return scan.nextInt();
	}
	
	//Main Method
	public static void main(String[] args) {
		//declaring variables to store the user inputs
		int area, time;
		//getting user input for area code
		area = getAreaCode();
		//getting user input for call length
		time = getCallLength();
		//variable to store the total cost
		double cost=0;
		//money formatting
		DecimalFormat dform = new DecimalFormat("###,###,###,##0.00");
		
		//loop to check the price of the area code entered
		for(int i=0; i<areaCode.length;i++) {
			if(areaCode[i] == area)
				cost = pMR[i] * time;
		}
		//sees if the area code entered was valid
		if(cost == 0)
			System.out.println("Your area code was invalid");
		else
			//displays result
			System.out.println("Your call to area code "+ area +" that was "+time+" minutes long cost a total of $" + dform.format(cost));
	}
}
