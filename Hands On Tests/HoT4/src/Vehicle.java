
public class Vehicle {
	// field for the speed of the Vehicle
	protected int speed=0;
	//default constructor
	public Vehicle() {
		
	}
	//method to increase Vehicle speed
	public void accelerate() {
		speed +=5;
	}
	//getter for speed
	public int getSpeed() {
		return speed;
	}
	//setter for speed
	public void setSpeed(int speed) {
		this.speed = speed;
	}
}
