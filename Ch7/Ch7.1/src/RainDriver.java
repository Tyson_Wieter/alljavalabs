import java.util.Scanner;
public class RainDriver {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the rainfall for each month seperated by pressing enter");
		double[] rainfall = {scan.nextDouble(),scan.nextDouble(),scan.nextDouble(),scan.nextDouble(),scan.nextDouble(),scan.nextDouble(),scan.nextDouble(),scan.nextDouble(),scan.nextDouble(),scan.nextDouble(),scan.nextDouble(),scan.nextDouble()};
		Rainfall rain = new Rainfall(rainfall);
		System.out.println("Total Rainfall for the year: " +rain.rainForYear());
		System.out.println("Average Rainfall per month for the year: " +rain.avgRain());
		System.out.println("Month with the highest amount of rain: " +rain.highRain());
		System.out.println("Month with the lowest amount of rain: " +rain.lowRain());
		
	}
}
