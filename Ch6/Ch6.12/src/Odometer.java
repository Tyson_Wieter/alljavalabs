
public class Odometer {
	private int mileage;
	private int startMileage = mileage;
	FuelGauge gas = new FuelGauge(0);
	public Odometer(int mileage) {
		this.mileage = mileage;
	}
	public int getMileage() {
		return mileage;
	}
	public void setMileage(int mileage) {
		this.mileage = mileage;
	}
	public void addMile() {
		if (mileage < 999999)
			mileage++;
		else
			System.out.println("Mileage is already at its maximum");
	}
	public void fillTank() {
		while(gas.getFuel() <15) 
			gas.addFuel();
	}
	public void drive24() {
		if (mileage == startMileage+24) {
			gas.removeFuel();
			startMileage += 24;
		}
	}
	public void drive() {
		while(gas.getFuel() >0) {
			addMile();
			drive24();
			System.out.println("Mileage: "+getMileage());
			System.out.println("Gas: "+gas.getFuel());
			System.out.println();
		}
	}
}
