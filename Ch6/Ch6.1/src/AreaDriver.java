import java.text.DecimalFormat;
public class AreaDriver {
	public static void main(String[] args) {
		DecimalFormat dForm = new DecimalFormat("###,###,##0.00");
		double height = 2, length=2, width=2, radiusCircle=2, radiusCylinder=2;
		System.out.println(dForm.format(Area.shape(radiusCircle)));
		System.out.println(dForm.format(Area.shape(length, width)));
		System.out.println(dForm.format(Area.shape(radiusCylinder, height, Math.PI)));
	}
}
