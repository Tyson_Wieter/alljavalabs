import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.text.DecimalFormat;
public class Tax {
	public static class MyFirstGUI extends Application
	{
		private Label lab1,lab2,lab3,lab4,lab5;
		private TextField tField,tField2,tField3,tField4;
		DecimalFormat dform = new DecimalFormat("###,###,###,##0.00");
		public static void main(String[] args)
	    {
			launch(args);
	    }
	    @Override
	    public void start(Stage primaryStage)
	    {
	    	primaryStage.setTitle("Name Formatter");
	    	tField = new TextField("Value");
	    	Button but1 = new Button("Submit");
	    	lab1 = new Label("Assessment Value: ");
	    	lab2 = new Label("Property Tax: ");
	    	but1.setOnAction(handle1);
	    	VBox vbox = new VBox(tField, but1,lab1,lab2);
	    	Scene scene = new Scene(vbox, 200, 100);
	    	primaryStage.setScene(scene);
	    	primaryStage.show();
	    }
	    final EventHandler<ActionEvent> handle1 = new EventHandler<ActionEvent>() {
	    	@Override
			  public void handle(ActionEvent textChange)
			  {
	    		double cost = Double.parseDouble(tField.getText());
	    		lab1.setText(lab1.getText()+"$"+dform.format(cost*.60));
	    		lab2.setText(lab2.getText()+"$"+dform.format(((cost*.60)/100*0.64)));
			  }
	    };
    }
}
