import java.util.Scanner;

class Challenge12{
	public static void main(String[] args){		
		Scanner scan = new Scanner(System.in);
		String city;

		city = scan.nextLine();
		System.out.println(city.length());
		System.out.println(city.toUpper());	
		System.out.println(city.toLower());
		System.out.println(city.substring(0,1));	
	}
}