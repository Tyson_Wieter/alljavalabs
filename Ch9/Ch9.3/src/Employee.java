
public class Employee {
	private String name;
	private String employeeNumber;
	private String hireDate;
	
	public Employee(String n, String num, String date) {
		this.name = n;
		this.employeeNumber = num;
		this.hireDate = date;
	}
	Employee(){
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getHireDate() {
		return hireDate;
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}
	private boolean isValidEmpNum(String e) {
		return true;
	}
	public String toString() {
		return "";
	}
}
