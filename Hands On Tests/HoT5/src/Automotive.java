import java.text.DecimalFormat;

import javax.swing.JOptionPane;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Automotive {
	public static class MyFirstGUI extends Application
	{
		protected static final Exception NumberFormatException = null;
		private Label lbl1,lbl2,lbl3,lbl4,lbl5;
		private TextField tField,tField2;
		private CheckBox ch1,ch2,ch3,ch4,ch5,ch6,ch7;
		DecimalFormat dform = new DecimalFormat("###,###,###,##0.00");
		public static void main(String[] args)
	    {
			launch(args);
	    }
	    @Override
	    public void start(Stage primaryStage)
	    {
	    	primaryStage.setTitle("Ranken's Automotive Maintenance");
	    	tField = new TextField("Value");
	    	Button but1 = new Button("Calculate Charges");
	    	Button but2 = new Button("Exit");
	    	lbl1 = new Label("Routine Services");
	    	lbl2 = new Label("Nonroutine Services");
	    	lbl3 = new Label("Parts Charges:   ");
	    	lbl4 = new Label("Hours of Labor: ");
	    	lbl5 = new Label("");
	    	ch1 = new CheckBox("Oil Change ($26.00)");
	    	ch2 = new CheckBox("Lube Job ($18.00)");
	    	ch3 = new CheckBox("Radiator flush ($30.00)");
	    	ch4 = new CheckBox("Transmission flush ($80.00)");
	    	ch5 = new CheckBox("Inspection ($15.00)");
	    	ch6 = new CheckBox("Muffler Replacement ($100.00)");
	    	ch7 = new CheckBox("Tire Rotation ($20.00)");
	    	tField = new TextField();
	    	tField2 = new TextField();
	    	but1.setOnAction(handle1);
	    	but2.setOnAction(handle2);
	    	HBox hbox = new HBox(10,lbl3,tField);
	    	HBox hbox2 = new HBox(10,lbl4,tField2);
	    	HBox hbox3 = new HBox(10,but1,but2);
	    	VBox vbox = new VBox(15,lbl1,ch1,ch2,ch3,ch4,ch5,ch6,ch7,lbl5,lbl2,hbox,hbox2,hbox3);
	    	Scene scene = new Scene(vbox, 300, 450);
	    	primaryStage.setScene(scene);
	    	primaryStage.show();
	    }
	    final EventHandler<ActionEvent> handle1 = new EventHandler<ActionEvent>() {
	    	@Override
			  public void handle(ActionEvent textChange)
			  {
	    		double total = 0;
	    		double textBox1 = 0;
	    		double textBox2 = 0;
	    		try {
	    			if (!tField.getText().equals(""))
	    				textBox1 = Double.parseDouble(tField.getText());
	    		}
	    		catch (NumberFormatException e) {
	    			JOptionPane.showMessageDialog(null, "Please Enter a Number or Leave the Box Empty");
	    		}
	    		try {
	    			if (!tField2.getText().equals(""))
	    				textBox2 = Double.parseDouble(tField2.getText());
	    		}
	    		catch (NumberFormatException e) {
	    			JOptionPane.showMessageDialog(null, "Please Enter a Number or Leave the Box Empty");
	    		}
	    		if(ch1.isSelected())
	    			total += 26;
	    		if(ch1.isSelected()) {
	    			throw new IllegalArgumentException("Description is an empty string.");
	    		}
	    		if(ch2.isSelected())
	    			total += 18;
	    		if(ch3.isSelected())
	    			total += 30;
	    		if(ch4.isSelected())
	    			total += 80;
	    		if(ch5.isSelected())
	    			total += 15;
	    		if(ch6.isSelected())
	    			total += 100;
	    		if(ch7.isSelected())
	    			total += 20;
	    		if (!tField.getText().equals(""))
	    			total += textBox1;
	    		if (textBox2 >= 1)
	    			total += 20*textBox2;
	    		Alert alert = new Alert(AlertType.INFORMATION);
	    		alert.setTitle("Message Here...");
	    		alert.setContentText("TotalCharges: $"+dform.format(total));
	    		alert.showAndWait();
			  }
	    };
	    final EventHandler<ActionEvent> handle2 = new EventHandler<ActionEvent>() {
	    	@Override
			  public void handle(ActionEvent textChange)
			  {
	    		Platform.exit();
			  }
	    };
    }
}
