import java.util.Scanner;
import java.text.DecimalFormat;
public class Pennies4Pay {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		DecimalFormat money = new DecimalFormat("###,###,##0.00");
		int days=-1;
		double salary = 0.01;
		
		System.out.println("Enter the amount of days worked");
		while (days < 1)
			days = scan.nextInt();
		for (int i=1; i<days;i++) {
			salary += salary;
		}
		System.out.println(money.format(salary));
	}
}
