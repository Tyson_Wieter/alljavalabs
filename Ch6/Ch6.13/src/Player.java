
public class Player {
	Dice d6 = new Dice(6);
	private int points = 50;
	public int rollDie() {
		int roll = d6.roll();
		System.out.print("Roll "+roll +" | Points ");
		points -= roll;
		if (points < 0) {
			points = points*-1;
		}
		return points;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
}
