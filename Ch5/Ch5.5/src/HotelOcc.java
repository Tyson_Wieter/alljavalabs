import java.util.Scanner;
public class HotelOcc {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int floors, rooms, occupied, totalRooms=0, totalOcc=0;
		
		System.out.println("How many floors does the hotel have");
		floors = scan.nextInt();
		for (int i =0; i<floors; i++) {
			System.out.println("How many rooms are on floor "+i+"?");
			rooms = scan.nextInt();
			System.out.println("How many are occupied?");
			occupied = scan.nextInt();
			totalRooms += rooms;
			totalOcc += occupied;
		}
		System.out.println("Rooms: "+totalRooms);
		System.out.println("Occupied: "+totalOcc);
		System.out.println("Vacant: "+(totalRooms - totalOcc));
		System.out.println("Occupancy: "+(totalRooms/totalOcc));
	}
}
