
public class RoomDimension {
	private double length, width;
	
	public RoomDimension(double length, double width) {
		this.length = length;
		this.width = width;
	}
	
	
	public double area() {
		return length*width;
	}
	
}
