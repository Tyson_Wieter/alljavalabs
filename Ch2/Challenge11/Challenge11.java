import java.util.Scanner;

class Challenge11{
	public static void main(String[] args){		
		Scanner scan = new Scanner(System.in);
		double boys, girls, pBoys, pGirls;

		System.out.println("Enter the number of boys and girls seperated by pressing enter");
		boys = scan.nextDouble();
		girls = scan.nextDouble();
		pBoys = boys/(boys+girls)*100;
		pGirls = girls/(boys+girls)*100;
		System.out.printf("Boys: %.0f%%%n",pBoys);
		System.out.printf("\nGirls: %.0f%%%n",pGirls);
	}
}