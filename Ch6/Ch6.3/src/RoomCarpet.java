
public class RoomCarpet {
	
	RoomDimension size;
	private double carpetCost;
	
	
	
	public RoomCarpet(RoomDimension dim,double cost)
	{
	   size = dim;
	   carpetCost = cost;
	}
			
	
	public double totalCost() {
		return carpetCost * size.area();
	}
}
