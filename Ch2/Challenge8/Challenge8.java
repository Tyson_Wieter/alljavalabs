import java.util.Scanner;

class Challenge8{
	public static void main(String[] args){
		//Initialize variables and scanner
		final double STATETAX = 0.055;
		final double COUNTYTAX = 0.02;
		double purchase;
		double cTax, sTax, tTax, tSale;
		Scanner scan = new Scanner(System.in);

		//Getting inputs for all of the different items and name
		System.out.println("Enter the amount of the sale");
		purchase = scan.nextDouble();

		//Doing math to calculate the total
		cTax = purchase*COUNTYTAX;
		sTax = purchase*STATETAX;
		tTax = (COUNTYTAX*purchase)+(STATETAX*purchase);
		tSale = (purchase+(COUNTYTAX*purchase)+(STATETAX*purchase));

		//Outputting results
		System.out.printf("\nPurchase amount: $%.2f", purchase);
		System.out.printf("\nState Tax: $%.2f", sTax);
		System.out.printf("\nCounty Tax: $%.2f", cTax);
		System.out.printf("\nTotal Tax: $%.2f", tTax);
		System.out.printf("\nTotal Sale: $%.2f", tSale);
	}
}