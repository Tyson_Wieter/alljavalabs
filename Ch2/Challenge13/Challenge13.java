import java.util.Scanner;

class Challenge13{
	public static void main(String[] args){		
		Scanner scan = new Scanner(System.in);
		double meal, tax, total, tip;

		System.out.println("Enter the price of the meal");
		meal = scan.nextDouble();
		tax = meal*0.075;
		tip = meal*0.18;
		total = meal+tax+tip;
		System.out.println("Meal Charge: "+meal);
		System.out.println("Tax Amount: "+tax);
		System.out.println("Tip Amount: "+tip);
		System.out.println("Total Amount: "+total);
	}
}